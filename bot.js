const Discord = require('discord.js');   // Подключаем библиотеку дискорда / Connecting the Discord library
const bot = new Discord.Client();        // Создаём клиент бота            / Creating the bot’s client
const config = require('config.json');   // Подключаем конфиг              / Connecting config
const fs = require('fs');                // Подключаем редактор файлов     / Connecting file editor

let token = config.token;                // Получаем токен из конфига      / Receiving a token from the config
let pref = config.Prefix;                // Получаем префикс команд из конфига / Receiving commands prefix from the config

bot.commands = new Discord.Collection(); // Создаём колекцию команд бота    / Create a collection of bot commands

fs.readdir('./cmds/', (err, files) => {  // Чтения команд из папки ./cmds/     / Commands’ readings from the ./cmds/ folder
  if (err) console.log(err);
  let jsFiles = files.filter(f => f.split(".").pop() === "js");
  if (jsFiles.length <= 0) {
    console.log("Missing download commands / Отсутствуют команды для загрузки");
    return;
  }
  else console.log(`Loaded ${jsFiles.length} command`);
  jsFiles.forEach((f,i) => 
  {
      let props = require(`./cmds/${f}`);
      console.log(`${i+1}.${f} loaded.`);
      bot.commands.set(props.help.name, props); 
  });
});

bot.login(token);                               // Авторизуем бота     / Authorizing the bot

bot.on('ready', () => {                         // Проверяем работоспособность бота / Checking the bot’s functionality
    console.log(`Fbay-Public-Bot activated`);
  });

bot.on('message', async msg =>                  // Обработчик новых сообщений / New messages processor
  {
      if (!msg.content.startsWith(pref)) return;// Игнор сообщений начинающихся не с префикса. / Ignoring messages starting not with the prefix
      if (msg.author.bot) return;               // Игнорирование сообщений от ботов  / Ignoring bots’ messages
      
      let msgArray = msg.content.split(" ");    // Делим сообщение по пробелам   / Splitting the message by spaces
      let command = msgArray[0].toLowerCase();  // Выделяем команду   / Singling out the command
      let args = msgArray.slice(1);             // Отделяем остальную часть сообщения (или параметры) от команд / Separating the rest of the message (or parameters) from the command

      let cmd = bot.commands.get(command.slice(pref.length));  // Получаем вызванную команду.    / Receiving the triggered command
      if(cmd) cmd.run(bot, msg, args);          // Вызываем метод команды.        / Triggering the command method
  });
 



